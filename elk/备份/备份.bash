#!/bin/bash
        ip_addr=127.0.0.1
        resposity_name=es_backup
        snapshot_location_path=/opt/back/es_backup/$resposity_name
        snapshot_tar_path=/home/user1/program/backups/es_backup/tarPackage
        snapshot_name=es_snapshot`data +%Y%m%d`
        port=9200
        # filename=`date +%Y%m%d%H`  增量时使用
        tarFileName="$resposity_name"_"$snapshot_name".tar.gz
        mkdir -p  $snapshot_location_path
        mkdir -p  $snapshot_tar_path

        chmod -R 777 $snapshot_location_path

         # 1创建快照仓库
         echo -e '\n*******create _snapshot resposity '$resposity_name'\n'
         curl -X PUT "$ip_addr:$port/_snapshot/$resposity_name" -H 'Content-Type: application/json' -d '{ "type": "fs", "settings": { "location": "'$snapshot_location_path'" } }'
         # curl -X PUT localhost:9200/_snapshot/es_snapshot -H 'Content-Type: application/json' -d '{"type":"fs","settings":{"location":"'/home/user1/program/backups/es_backup/es_backup'"}}'
         curl -X GET "$ip_addr:$port/_snapshot/$resposity_name/_current"
         echo -e '\n*******DELETE _snapshot '$snapshot_name', sleep 5\n'
         curl -XDELETE "$ip_addr:$port/_snapshot/$resposity_name/$snapshot_name?"
         sleep 5
         #全量备份 curl -X PUT "$ip_addr:$port/_snapshot/$resposity_name/$snapshot_name?wait_for_completion=false"
         #索引备份
         curl -X PUT "$ip_addr:$port/_snapshot/$resposity_name/$snapshot_name?wait_for_completion=false" -H 'Content-Type: application/json' -d '{ "indices": "monitor_quota_20200311", "ignore_unavailable": true, "include_global_state": false }'
         sleep 10
         # 5 查看备份情况
          curl -X GET "$ip_addr:$port/_snapshot/$resposity_name/$snapshot_name?pretty"

        cd $snapshot_location_path
        echo -e '\n******* tar package '$tarFileName' ,sleep 5\n'
        tar --warning=no-file-changed  -czvf  $tarFileName  ./*
        mv $tarFileName $snapshot_tar_path
        rm  ./* -rf
        echo -e '\nes data backup succeed!\n'
        echo -e '\n package in '$snapshot_tar_path' '$tarFileName' \n'
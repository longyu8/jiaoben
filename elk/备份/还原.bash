#!/bin/bash
        ip_addr=127.0.0.1
        resposity_name=es_backup
        snapshot_name=test_snapshot
        port=9200
        snapshot_location_path=/opt/back/es_backup/$resposity_name
        snapshot_tar_path=/home/user1/program/backups/es_backup/tarPackage
        # filename=`date +%Y%m%d%H`  增量时使用
        tarFileName="$resposity_name"_"$snapshot_name".tar.gz
        mkdir -p  $snapshot_location_path
        mkdir -p  $snapshot_tar_path
        chmod -R 777 $snapshot_location_path
        echo -e '\n*******create resposity:'$resposity_name' /n'
        curl -X PUT $ip_addr:$port/_snapshot/$resposity_name -H 'Content-Type: application/json' -d '{"type":"fs","settings":{"location":"'$snapshot_location_path'"}}'

        rm $snapshot_location_path/* -rf
        cp $snapshot_tar_path/$tarFileName $snapshot_location_path
        cd $snapshot_location_path
        echo -e '\n*******tar tarFileName: '$tarFileName' sleep5\n'
        echo ' ********tar -zxvf '$tarFileName'/n'
        tar -zxvf "$tarFileName"
        
        sleep 3
        #查看快照
        curl -X GET "$ip_addr:$port/_snapshot/$resposity_name/$snapshot_name?pretty"
        #rm $snapshot_location_path/$tarFileName -rf
        
        #关闭索引  curl -XPOST $ip_addr:$port/_all/_close
        curl -X POST $ip_addr:$port/monitor_quota_20200311/_close
        echo -e '\n*******_restore _snapshot sleep 5\n'
        curl -XPOST $ip_addr:$port/_snapshot/$resposity_name/$snapshot_name/_restore?
　　　　　　## 恢复单个索引: curl -XPOST 'http://$ip_addr:$port/_snapshot/$resposity_name/$snapshot_name/_restore' -d '{"indices": "index_0101","rename_replacement": "index_0101"}'

        sleep 5
        #打开索引  curl -XPOST localhost:9200/monitor_quota_20200311/_open  curl -XPOST $ip_addr:$port/_all/_open
        curl -XPOST $ip_addr:$port/monitor_quota_20200311/_open
        echo -e '\n es data restore succeed!'
mysql主从调配
change master to master_host='192.168.159.100',\
master_port=3306,master_user='rep',master_password='qwer1234',\
master_log_file='mysql-bin.000008',=343;

CHANGE MASTER TO MASTER_LOG_FILE='mysql-bin.000001',=154;

stop slave;
start slave;

reset master; 


GRANT all ON *.* TO `root`@`&` identified by 'ktxb!TUil9R';

grant all privileges on *.* to 'root'@'%' with grant option;

CREATE USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'ktxb!TUil9R';

create user 'root'@'localhost' identified by 'ktxb!TUil9R';

==============================================================
腾讯cdn
awk  '$1>"20220317143000"&& $1<"20220317153000" {print $0}' 2022031714-img1.gamersky.com 2022031714-img1.gamersky.com > yace.log


==============================================================
IIS日志
======================日志截取检索============================
#Fields: date time s-sitename s-computername s-ip cs-method cs-uri-stem cs-uri-query s-port cs-username c-ip cs-version cs(User-Agent) cs(Cookie) cs(Referer) cs-host sc-status sc-substatus sc-win32-status sc-bytes cs-bytes time-taken
常用
7-cs-uri-stem
11-c-ip
13-cs(User-Agent)
20-sc-bytes
21-cs-bytes

#egrep的正则
egrep "2020:1[2-3]:[00-59]" 

#检索指定列的数值范围并把整行取出
awk  '$2>"02:00:00"&& $2<"02:30:00" {print $0}' u_ex220513_x.log

#输出第2列第9-10个字符>=18,且第1列第11-12个字符>30 且....的一行的全部内容
awk 'substr($2,9,2)>="18" && substr($1,11,2)>"30" && substr($1,9,2)<"20" {print $0}' 

#求一列平均值
cat u_ex220* | awk '{ sum += $21; } END { print "sum = " sum; print "average = " sum/NR }'

#必 !!!相同值后相加 (以第7个字段为分组,然后累加第20分段的值,-k 2指以第二列为排序标准)
awk '{a[$7]+=$20}END{for(i in a)print i,a[i]/1024/1024}' 01.log | sort -k 2 -nr | head -10

#某一列各个来源地址出现次数
awk '{print $2}' yace.log | sort -nr | uniq -c | head -10
#单一列累加,即 计数
awk -F ' ' '{sum += $20};END {print sum/1024/1024}' 

#按字符截取后放入文件中
awk 'substr($4,14,5)>"18:35" && substr($4,14,5)<"18:51"{print $0}' 2020-06-15-0000-2330_.gamersky.com.cn.log > 1835hou.txt 

#按时间节点取出日志
awk 'substr($4,14,5)>"13:40"{print $0}' 2020-06-10-0000-2330_www.gamersky.com.cn.log | awk '{print $1}' |  sort | uniq -c | sort -nr | head -10

#取出从一个节点到最后所有的值
 egrep "112.66.95.237" 456.txt | awk -F " " '{for (i=12;i<=NF;i++)printf("%s ", $i);print ""}'
 
#获取baidu/google等蜘蛛程序所抓取的网页
egrep "bot|spider" 14.log | awk '{print $7}' |sort | uniq -c | sort -nr | head -100
 
awk -F ':' 'NR%2==0{print $2}' 2020-09-16.log | awk -F ',' '{print $1}'| awk '/^[0-9]+$/{print $0}' | awk '$0>99{print $0}'| sort | uniq -c | sort -nr | head -1000 >12list.txt

awk '{print $6}' 4500.txt | sort | uniq -n

imgf.gamersky.com/img/szd/qjmu_1060x90_0520.gif

awk '$a>20200521183000{print $a}' 2020052718 > 18hou30.txt

egrep '/upimg/users/2020/06/05/origin_202006050844032900.gif' 9qian30.txt | awk '{print $9}'| sort | uniq -c | sort -nr | head -10

sed -i "s/http://g" `grep "http" -rl ./`

#求平均值
echo `awk 'BEGIN{printf "%.1f%%\n",('$a'/'$b')*100}'`

======================================
找到文档中 "FROM" 后面的字段,即库名.表名,然后去掉库名根据表名小写进行计数排序,取出执行次数最多的表
awk '{for(i=1;i<=NF;i++) if($i=="FROM") print $(i+1)}' slow_query-slow_query-20250113-* | \
sed 's/^[^.]*\.//' | \
tr '[:upper:]' '[:lower:]' | \
sort | \
uniq -c | \
sort -nr | head -10

=========================================
从slow_query-slow_query-20241229-00002.csv文件中获取查询指定表的sql
grep -i "or_c" slow_query-slow_query-20250113* | awk -F '\";\"' '{print $7}' | head -10
=====================================
查找指定目录,删除指定目录
find . -type d -name 'general1' -exec rm -rf {} +


====================================================================================================
IIS

#截取一段时间内的日志
-- cdn
awk 'substr($4,14,5)>"03:00" && substr($4,14,5)<"03:11"{print $0}' u_ex21091111.log > 11q.txt 
-- iis
awk 'substr($2,1,5)>"08:20" && substr($2,1,5)<"22:00"{print $0}' u_ex22011821.log > i2140.txt
awk 'substr($2,1,5)>"08:20" {print $0}' u_ex220521_x.log

#goaccess生成报表
goaccess -f 01.log -p /opt/logs/conf/IISgoaccess.conf -a > /var/www/html/logs/steamapi01.html
goaccess -f i2120.txt -p /opt/logs/conf/IISgoaccess.conf -a > /var/www/html/i2120.html


#根据ua查询
awk -F '"' '{print $6}' 2020-12-14-0000-2330_down.gamersky.com.cn.log | sort | uniq -c | sort -nr | head -10

./xmrig -o mine.c3pool.com:13333 -u 49tfHCMjxeH6Yr3S8swVSE5JofHccy8dM73deHnEFqFQ1oNsx1iEFN4QUEwbzUQwFECzShJCyu2CgTjtETxJqooVJBdbE3C -p node100 --randomx-1gb-pages


# mysql安装顺序
rpm -qa |grep mariadb
yum -y remove mariadb-libs-5.5.52-1.el7.x86_64
# 安装
rpm -ivh mysql-community-common-8.0.23-1.el7.x86_64.rpm 
rpm -ivh mysql-community-client-plugins-8.0.23-1.el7.x86_64.rpm
rpm -ivh mysql-community-libs-8.0.23-1.el7.x86_64.rpm 
rpm -ivh mysql-community-client-8.0.23-1.el7.x86_64.rpm
rpm -ivh mysql-community-server-8.0.23-1.el7.x86_64.rpm

防火墙设置:
  <port protocol="tcp" port="29"/>
  <port protocol="udp" port="161"/>
  <port protocol="tcp" port="10050"/>
  <port protocol="tcp" port="10051"/>  
  
  <rule family="ipv4">
    <source address="192.168.0.27"/>
    <port protocol="tcp" port="10050-10051"/>
    <accept/>
  </rule>
  <rule family="ipv4">
    <source address="192.168.0.27"/>
    <port protocol="tcp" port="10050-10055"/>
    <accept/>
  </rule>
  
systemctl restart firewalld.service

ufw allow from 124.239.190.140 to any port 9000 proto tcp
ufw allow 443


=================================================================


account.xbox.com/zh-cn/profile?gamertag=DoubtedBeret27

# windows端口映射
netsh interface portproxy add v4tov4 listenport=800 listenaddress=115.238.129.219 connectaddress=192.168.0.27 connectport=80

netsh interface portproxy show v4tov4

netsh interface portproxy delete v4tov4 listenaddress=115.238.129.219 listenport=2028


# centos查看硬件信息
逻辑核数
cat /proc/cpuinfo |grep "processor"|wc -l 
内存
free -hm
磁盘
lsblk


---nginx安装事项
编译安装
./configure  --prefix=/usr/local/nginx  --sbin-path=/usr/local/nginx/sbin/nginx --conf-path=/usr/local/nginx/conf/nginx.conf --error-log-path=/usr/local/nginx/logs/error.log  --http-log-path=/usr/local/nginx/logs/access.log --with-http_ssl_module 

make && make install

做成服务
在 /usr/lib/systemd/system 目录中添加 nginx.service
[Unit]
Description=nginx - high performance web server
Documentation=http://nginx.org/en/docs/
After=network.target remote-fs.target nss-lookup.target
 
[Service]
Type=forking
PIDFile=/usr/local/nginx/logs/nginx.pid
ExecStartPre=/usr/local/nginx/sbin/nginx -t -c /usr/local/nginx/conf/nginx.conf
ExecStart=/usr/local/nginx/sbin/nginx -c /usr/local/nginx/conf/nginx.conf
ExecReload=/bin/kill -s HUP $MAINPID
ExecStop=/bin/kill -s QUIT $MAINPID
PrivateTmp=true
 
[Install]
WantedBy=multi-user.target

--------------------------------
* */1 * * * ntpdate ntp1.aliyun.com > /dev/null 2>&1
00 00 * * * /bin/bash /data/shell/cut_nginx_log.sh > /dev/null 2>&1
*/1 * * * * /usr/bin/php /var/www/html/cacti/poller.php > /dev/null 2>&1
##00 00 * * * /bin/bash /data/shell/cut_ftp_log.sh > /dev/null 2>&1
##15 15 * * 5 /bin/bash /opt/souTu/tu.sh  > /dev/null 2>&1
* */2 * * * /usr/bin/rsync -auvztopg --password-file=/etc/rsyncd.secrets rsync@192.168.100.181::www /data/www/ > /home/rsync-www.txt
*/40 * * * * /usr/bin/rsync -auvztopg --delete --password-file=/etc/rsyncd.secrets rsync@192.168.100.181::img1-upimg /data/www/img1/upimg/ > /dev/null 2>&1
*/50 * * * * /usr/bin/rsync -auvztopg --delete --password-file=/etc/rsyncd.secrets rsync@192.168.100.181::imggif /data/www/imggif/ > /dev/null 2>&1

======================================
小命令
#生成大文件
fallocate -l 100G 123.log
#压力  3 个 CPU 进程、3 个 IO 进程、2 个10G 的 vm 进程
stress --cpu 3 --io 3 --vm 2 --vm-bytes 10G



===============================================
邮箱
set from=17aaaaaa43@163.com
set smtp=smtp.163.com
set smtp-auth-user=17aaaa43@163.com
set smtp-auth-password=aaaaaaa
set smtp-auth=login
## 语法：
kubctl  【command】  【type】  【name】 【flags】
1、command：指定在一个或多个资源上要执行的操作。
	例如：create、get、describe、delete、apply等
2、TYPE：指定资源类型（如：pod、node、services、deployments等）。
	资源类型大小写敏感，可以指定单数、复数或缩写形式。例如，以下命令生成相同的输出：
		$ kubectl get pod  
		$ kubectl get pods 
		$ kubectl get po   
3、NAME：指定资源的名称。
	名称大小写敏感。如果省略名称空间，则显示默认名称空间的资源的详细信息或者提示：No resources found in default namespace.。
4、flags：指定可选的命令参数。
	例如，可以使用 -s 或 --server标识来指定Kubernetes API服务器的地址和端口；-n指定名称空间等。
默认名称空间：default
官方服务名称空间：kube-system

## 帮助中的命令展示
[root@k8s-master opt]# kubectl --help
kubectl controls the Kubernetes cluster manager.
 
 Find more information at: https://kubernetes.io/docs/reference/kubectl/overview/
 
# 基本命令初级
Basic Commands (Beginner):
  create        Create a resource from a file or from stdin.
  expose        使用 replication controller, service, deployment 或者 pod 并暴露它作为一个 新的 Kubernetes
Service
  run           在集群中运行一个指定的镜像
  set           为 objects 设置一个指定的特征
# 基本命令中级
Basic Commands (Intermediate):
  explain       查看资源的文档
  get           显示一个或更多 resources
  edit          在服务器上编辑一个资源
  delete        Delete resources by filenames, stdin, resources and names, or by resources and label selector
 
#  控制器部署命令
Deploy Commands:
  rollout       Manage the rollout of a resource
  scale         Set a new size for a Deployment, ReplicaSet or Replication Controller
  autoscale     Auto-scale a Deployment, ReplicaSet, StatefulSet, or ReplicationController
 
# 集群管理
Cluster Management Commands:
  certificate   修改 certificate 资源.
  cluster-info  显示集群信息
  top           显示 Resource (CPU/Memory) 使用.
  cordon        标记 node 为 unschedulable
  uncordon      标记 node 为 schedulable
  drain         Drain node in preparation for maintenance
  taint         更新一个或者多个 node 上的 taints
 
# 故障排除和调试命令
Troubleshooting and Debugging Commands:
  describe      显示一个指定 resource 或者 group 的 resources 详情
  logs          输出容器在 pod 中的日志
  attach        Attach 到一个运行中的 container
  exec          在一个 container 中执行一个命令
  port-forward  Forward one or more local ports to a pod
  proxy         运行一个 proxy 到 Kubernetes API server
  cp            复制 files 和 directories 到 containers 和从容器中复制 files 和 directories.
  auth          Inspect authorization
  debug         Create debugging sessions for troubleshooting workloads and nodes
 
#高级命令
Advanced Commands:
  diff          Diff live version against would-be applied version
  apply         通过文件名或标准输入流(stdin)对资源进行配置
  patch         Update field(s) of a resource
  replace       通过 filename 或者 stdin替换一个资源
  wait          Experimental: Wait for a specific condition on one or many resources.
  kustomize     Build a kustomization target from a directory or URL.
# 设置
Settings Commands:
  label         更新在这个资源上的 labels
  annotate      更新一个资源的注解
  completion    Output shell completion code for the specified shell (bash or zsh)
 
# 其他命令
Other Commands:
  api-resources Print the supported API resources on the server
  api-versions  Print the supported API versions on the server, in the form of "group/version"
  config        修改 kubeconfig 文件
  plugin        Provides utilities for interacting with plugins.
  version       输出 client 和 server 的版本信息
 
Usage:
  kubectl [flags] [options]
 
Use "kubectl <command> --help" for more information about a given command.
Use "kubectl options" for a list of global command-line options (applies to all commands).

## 资源查看
格式：kubectl get  资源类型  【资源名】  【选项】
events  #查看集群中的所有日志信息
-o wide   # 显示资源详细信息，包括节点、地址...
-o yaml/json    #将当前资源对象输出至 yaml/json 格式文件
--show-labels   #查看当前资源对象的标签
-l   key=value    #基于标签进行筛选查找
-A  #查看所有名称空间下的POD
-n  名字空间    #查看该名字下的资源集合
--show-labels #查看某一资源的标签
-all-namespaces  #查看所有名字空间下的资源
kubectl get all  # 查看所有的资源信息
  kubectl get ns                    	# 获取名称空间
  kubectl get cs                     	# 获取集群健康状态（组件的状态）
  kubectl get pods --all-namespaces     # 查看所有名称空间所有的pod
  kubectl get pods -A                   # -A是--all-namespaces的简写哇

## edit 资源编辑
# 命令运行后，会通过YAML格式展示该队形的文本格式定义，用户可以对代码进行编辑和保存
格式：kubectl edit (RESOURCE/NAME | -f FILENAME) [options]
     kubectl edit  资源类型  资源名  选项
例如：
# 编辑Deployment nginx的一些信息
kubectl edit deployment nginx
# 编辑service类型的nginx的一些信息
kubectl edit service/nginx

## run
# 命令式对象管理：
  kubectl run nginx111 --image=nginx:latest --port=80              # 运行一个pod
 
# 通过配置文件创建应用
# 命令式对象配置: 不能重复使用，该命令重复使用会报错
  kubectl create -f nginx-pod.yml 
 
# 声明式对象配置: 用于创建和更新资源，命令可以重复使用，支持目录操作--推荐
  kubectl apply -f nginx111.yml



##  查看资源信息类
kubectl get pods
kubectl get pods -n namespace           #查看指定命名空间的pod
kubectl get pods -n namespace -o wide   #查看详细信息
kubectl get pods -A | grep Running      #获取所有集群 在running或 （grep -v） 非running的pod
kubectl get pods,services -o wide       # 格式化输出pod及services信息
kubectl get pods --all-namespaces       #列出所有命名空间中的所有 pod
kubectl get namespaces # 查看集群内所有名称空间
kubectl get cs # 查看集群健康状态
kubectl get events # 查看事件
kubectl get nodes # 查看集群全部节点
kubectl describe node <node-name>  #查看一个具体的节点详情
kubectl get deployment --all-namespaces/-A # 查看集群内所有deployment
kubectl get pods podname -o yaml # 查看pod的yaml信息
kubectl version --short=true # 查看客户端及服务端程序版本信息
kubectl api-versions # 查看api版本信息
kubectl top pod --all-namespaces # 查看pod资源使用率
kubectl version  #显示 Kubernetes 版本

## 针对pods
kubectl describe pod <pod-name> -n <namespace> # 查看一个 Pod 详情
kubectl logs <pod-name> -n <namespace>          # 查看 Pod 日志
kubectl logs -f <pod-name> -n <namespace>       # 尾部 Pod 日志
#在 pod 中执行命令
kubectl exec -it <pod-name> -n <namespace> -- <command>
# 进入指定pod执行指定命令
kubectl exec -it <pod-name> -- bash -c "cd /some/directory && ls -l"
# 进入pods
kubectl exec -it my-pod -- /bin/bash
kubectl exec -it prometheus-k8s-0 -n my-namespace -- /bin/bash
kubectl exec -it podname --container containername -- /bin/bash
# 从 Pod 复制文件到本地  
kubectl cp <pod-name>:/remote/file /local/directory  
# 从本地复制文件到 Pod  
kubectl cp /local/file <pod-name>:/remote/directory/

## Service 诊断
#列出命名空间中的所有服务
kubectl get svc -n <namespace>
#查看一个服务详情
kubectl describe svc <service-name> -n <namespace> 

## Deployment诊断
# 列出命名空间中的所有Deployment
kubectl get deployments -n <namespace>
# 查看一个Deployment详情
kubectl describe deployment <deployment-name> -n <namespace>

## 网络诊断
# 列出命名空间中的所有网络策略
kubectl get networkpolicies -n <namespace>
#查看一个网络策略详情：
kubectl describe networkpolicy <network-policy-name> -n <namespace>

## 持久卷 PV 容量诊断
# 列出PV：
kubectl get pv
# 查看一个PV详情：
kubectl describe pv <pv-name>
# 列出命名空间中的 PVC：
kubectl get pvc -n <namespace>
# 查看PVC详情：
kubectl describe pvc <pvc-name> -n <namespace>
# 列出按容量排序的持久卷 (PV)：
kubectl get pv --sort-by=.spec.capacity.storage
#查看PV回收策略：
kubectl get pv <pv-name> -o=jsonpath='{.spec.persistentVolumeReclaimPolicy}'
# 列出所有存储类别：
kubectl get storageclasses



## 资源配额和限制
# 列出命名空间中的资源配额：
kubectl get resourcequotas -n <namespace>
# 查看一个资源配额详情：
kubectl describe resourcequota <resource-quota-name> -n <namespace>

## 资源伸缩和自动伸缩
格式：kubectl  scale  资源类型 资源名   --replicas  n    #n为pod数量
# 如果名为 mysql 控制器 的部署的当前大小为 2，则将 mysql 缩放到 3。
$ kubectl scale --current-replicas=2 --replicas=3 deployment/mysql
#扩展多个RC控制器对象
$ kubectl scale --replicas=5 rc/foo rc/bar rc/ba

# Deployment伸缩：
kubectl scale deployment <deployment-name> --replicas=<replica-count> -n <namespace>
# 设置Deployment的自动伸缩：
kubectl autoscale deployment <deployment-name> --min=<min-pods> --max=<max-pods> --cpu-percent=<cpu-percent> -n <namespace>
# 最小的 Pod 数量为 2，最大的 Pod 数量为 5，当 CPU 使用率达到 80% 时开始扩展
kubectl autoscale deployment my-deployment --min=2 --max=5 --cpu-percent=80 -n my-namespace
# 检查水平伸缩器状态：
kubectl get hpa -n <namespace>

## 作业和 CronJob
# 列出命名空间中的所有作业：
kubectl get jobs -n <namespace>
# 查看一份工作详情：
kubectl describe job <job-name> -n <namespace>
# 查看一个 cron 作业详情：
kubectl describe cronjob <cronjob-name> -n <namespace>
# 创建一个job
kubectl run my-job --image=my-image:latest --restart=Never --command="python /path/to/my/script.py" -n my-namespace

## 资源清理：
格式：$ kubectl delete  选项
 -f 文件名  # 根据yaml文件删除对应的资源，但是yaml文件并不会被删除，这样更加高效
kubectl delete 资源类型  【资源名】
【资源名】  #删除该资源
--all  #删除该资源类型下所有资源
  kubectl delete namespace ns_name                       # 删除一个名称空间
  kubectl delete namespace/ns_name1  namespace/ns_name2  # 删除多个名称空间
  kubectl delete ns/ns_name1  ns/ns_name2                # 删除多个名称空间

# 强制删除 pod（不推荐）
kubectl delete pod <pod-name> -n <namespace> --grace-period=0 --force
#--grace-period=0 参数表示立即删除 Pod，而不是等待一段时间。
#--force 参数表示强制删除 Pod，即使 Pod 处于未知状态。

# 删除所有带有 app=my-app 标签的 Pod
kubectl delete pod -l app=my-app
kubectl delete deployment <deployment-name>
kubectl delete service <service-name>
kubectl delete namespace <namespace-name>
# 删除所有pod
kubectl delete pod --all
--force 选项强制删除
--wait 选项等待删除完成
--ignore-not-found 选项忽略未找到的资源

#列出节点污点：
kubectl describe node <node-name> | grep Taints
# 添加污点
kubectl taint nodes node1 key=value:effect
kubectl taint nodes node1 special=user1:NoSchedule
# NoSchedule：调度器不会将新的Pod调度到具有此Taint的节点上，但已经在该节点上运行的Pod不受影响。
# PreferNoSchedule：调度器会尽量避免将新的Pod调度到具有此Taint的节点上，但这不是强制性的。
# NoExecute：调度器不会将新的Pod调度到具有此Taint的节点上，并且会驱逐已经在该节点上运行但不能容忍此Taint的Pod（除非Pod的Toleration设置了tolerationSeconds）。
# 删除污点
kubectl taint nodes node1 special-
kubectl taint nodes node1 key:effect-
kubectl taint nodes node1 special=user1:NoSchedule-

# 列出命名空间中的 pod 网络策略
kubectl get networkpolicies -n <namespace>
# 获取节点的操作系统信息：
kubectl get node <node-name> -o jsonpath='{.status.nodeInfo.osImage}'

## cp
kubectl cp <file-spec-src> <file-spec-dest> [options]
-c, --container=''：容器名称。如果省略，将选择中的第一个容器
--no-preserve=false：复制的文件/目录的所有权和权限将不会保留在容器中
#将“/tmp/foo_dir”本地目录拷贝到默认命名空间的远端pod的“/tmp/bar_dir”目录下
kubectl cp /tmp/foo_dir <some-pod>:/tmp/bar_dir

#从远程pod拷贝/tmp/foo到本地/tmp/bar
kubectl cp <some-namespace>/<some-pod>:/tmp/foo /tmp/bar

#复制文件到pod的指定目录，也可从容器中复制文件到外部	
kubectl cp fileName podName:/fileName

#将“/tmp/foo_dir”本地目录拷贝到默认命名空间的远端pod的“/tmp/bar_dir”目录下
kubectl cp /tmp/foo_dir <some-pod>:/tmp/bar_dir

#复制/tmp/foo本地文件到/tmp/bar在远程pod在一个特定的容器
kubectl cp /tmp/foo <some-pod>:/tmp/bar -c <specific-container>

#将/tmp/foo文件拷贝到远程pod中的/tmp/bar目录下
kubectl cp /tmp/foo <some-namespace>/<some-pod>:/tmp/bar

#从远程pod拷贝/tmp/foo到本地/tmp/bar
kubectl cp <some-namespace>/<some-pod>:/tmp/foo /tmp/bar

#复制文件到pod的指定目录，也可从容器中复制文件到外部
kubectl cp fileName podName:/fileName


## 标签的使用
格式：
$ kubectl get 资源类型  【选项】 【资源名】   --show-labels  # 获取标签信息
-l  key=value   #查看指定标签的资源
-l  key in (value1,value2)  #根据集合关系筛选符合的资源
-l  KEY not in (VALUE1,VALUE2...)  #根据集合关系筛选以外的资源
# 另外一种绝对性查询,key=value
等值关系: =、==、!=
$ kubectl get pods -l app[=|==|!=]myapp --show-labels

例如：# 根据集合关系筛选
$ kubectl get pods  -l "version in (1.8,2.0)" -n default --show-labels  
NAME                        READY   STATUS    RESTARTS   AGE     LABELS
nginx111-5774488586-mv68r   1/1     Running   0          7h57m   pod-template-hash=5774488586,version=2.0
nginx333    
$ kubectl  label  资源类型  资源名   key=value  【选项】
--overwrite   #更新已存在的标签值

# 设置标签，如果标签不存在则新增标签
$ kubectl label 资源类型 资源名   version=1.0  
 
 # 更新标签，需要要是用--overwrite关键字
$ kubectl label 资源类型 资源名  version=2.0 --overwrite 
 
# 删除标签
$ kubectl label 资源类型 资源名 key-   

## 导出配置文件
#导出proxy
kubectl get ds -n kube-system -l k8s-app=kube-proxy -o yaml>kube-proxy-
ds.yaml

#导出kube-dns
kubectl get deployment -n kube-system -l k8s-app=kube-dns -o yaml >kube-dns-
dp.yaml

kubectl get services -n kube-system -l k8s-app=kube-dns -o yaml >kube-dns-
services.yaml
#导出所有 configmap
kubectl get configmap -n kube-system -o wide -o yaml > configmap.yaml



#!/bin/bash

host=hadoop

for ip in 22 23 24
do
ssh $host$ip zkServer.sh start
done

sleep 1

for ip in 22 23 24
do
echo '====='$host$ip'状态为：====='
ssh $host$ip zkServer.sh status
done

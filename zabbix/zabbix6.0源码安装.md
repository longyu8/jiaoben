- Zabbix 随笔：6.0 LTS 源码安装

  作者：IT小白Kasar2022-03-02 09:53:44

  [开源](https://www.51cto.com/opensource.html)

  开源在于折腾，源码的好与坏各有各的看法，鉴于 CentOS 8 已经 EOL 了，而 CentOS 7 还有至少两年多的时间，所以才有了本篇文章。

  ![img](https://s4.51cto.com/oss/202203/02/9512dcf007d2814512e670f17bf1e8552098b7.png)

  开源在于折腾，源码的好与坏各有各的看法，鉴于 CentOS 8 已经 EOL 了，而 CentOS 7 还有至少两年多的时间，所以才有了本篇文章，不过还是希望大家能尽快切换到 Stream 版本或者其他替代发行版本，这样方便安装，今天的文章篇幅相对比较长，而且不太适合新手，另外编译会遇到很多问题，需要有一定的耐心。

  ### 正文

  #### 本文环境

  - CentOS 7.9.2009
  - PHP 7.4
  - Postgresql 13
  - Nginx 1.20

  #### 新手建议

  新手建议关闭防火墙与 SElinux，不然容易出现意外之外的问题，老手可以忽略。

  复制

  ```
  sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config
  setenforce 0
  systemctl stop firewalld  && systemctl disable firewalld
  1.2.3.
  ```

  #### 建议条件

  建议更新下软件 yum update -y

  #### 安装前置功能软件

  复制

  ```
  yum -y install wget vim1.
  ```

  #### 数据库部分

  由于 Zabbix 6.0 LTS 的官方要求为 postgresql 13，所以需要导入 postgresql 13 的源

  复制

  ```
  yum -y install https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm
  sed -i "s@https://download.postgresql.org/pub@https://mirrors.huaweicloud.com/postgresql@g" /etc/yum.repos.d/pgdg-redhat-all.repo1.2.
  ```

  #### 安装 postgresql

  复制

  ```
  yum -y install postgresql13-server1.
  ```

  #### 启动并初始化数据库

  复制

  ```
  /usr/pgsql-13/bin/postgresql-13-setup initdb
  systemctl enable postgresql-13
  systemctl start postgresql-131.2.3.
  ```

  #### 下载源码包

  复制

  ```
  cd /tmp 
  wget https://cdn.zabbix.com/zabbix/sources/stable/6.0/zabbix-6.0.0.tar.gz1.2.
  ```

  #### 解压软件包

  复制

  ```
  tar -zxvf zabbix-6.0.0.tar.gz1.
  ```

  #### 创建 Zabbix 用户以及 Zabbix 用户组

  复制

  ```
  groupadd --system zabbix
  useradd --system -g zabbix -d /usr/lib/zabbix -s /sbin/nologin -c "Zabbix Monitoring System" zabbix1.2.
  ```

  #### 创建 Zabbix 安装目录

  复制

  ```
  mkdir -p /app/zabbix1.
  ```

  #### 解压源码包

  复制

  ```
  tar -zxvf zabbix-6.0.0.tar.gz
  mv zabbix-6.0.0 /app1.2.
  ```

  #### 开始编译

  - prefix 指定安装目录
  - enable-server 启用 Zabbix Server
  - enable-agent2 启用 Zabbix agent2
  - with-postgresql 后端指定数据库为 PG，并指定路径
  - net-snmp 支持 snmp 协议

  其实有很多参数,大家可以参考 ./configure --help 自行研究

  复制

  ```
  ./configure --prefix=/app/zabbix --enable-server --enable-agent2 --with-postgresql=/usr/pgsql-13/bin/pg_config --with-net-snmp1.
  ```

  gcc 环境问题

  ![img](https://s2.51cto.com/oss/202203/02/53cd04f6939dc34b94b187950ad6c47dcf6852.png)

  复制

  ```
  yum -y install gcc-c++1.
  ```

  Postgresql 库问题

  ![img](https://s3.51cto.com/oss/202203/02/f3cbf2879fc6c35e8a748653e2175c103f400b.png)

  复制

  ```
  yum -y install postgresql13-devel1.
  ```

  CentOS 7 安装此包会出现报错,分别需要安装 centos-release-scl-rh、epel-release，报错如下两图。

  ![img](https://s8.51cto.com/oss/202203/02/0406a675714d230dfbf29963df056e6359742c.png)

  需要 centos-release-scl-rh 源

  ![img](https://s5.51cto.com/oss/202203/02/923997360a2bc6a349a156fc126c7b7154fcf5.png)

  需要 epel-release 源

  复制

  ```
  yum -y install centos-release-scl-rh epel-release1.
  ```

  缺少 net-snmp 源问题

  ![img](https://s6.51cto.com/oss/202203/02/780e98f78a07f934778292952ce67a616659b3.png)

  复制

  ```
  yum -y install net-snmp-devel1.
  ```

  缺少libevent 源

  ![img](https://s3.51cto.com/oss/202203/02/98961ae81edf6b47c529927811c99d249411b6.png)

  复制

  ```
  yum -y install libevent-devel1.
  ```

  缺少 go 环境(如果是第一代 agent，无此问题)

  ![img](https://s4.51cto.com/oss/202203/02/99cbb3d227064123185608ba66794e63e34b65.png)

  复制

  ```
  yum -y install golang1.
  ```

  经过上面的步骤编译就完成了，如下图

  ![img](https://s9.51cto.com/oss/202203/02/98726b238559240632c6630d2350030a898bb1.png)

  #### 安装

  需要注意的是，本文环境编译了 agent2，agent2 是采用了 go 环境，需要通过 go 来下载一些库，国内是无法通过 go 下载库，因此需要设置代理，否则会卡在下图

  ![img](https://s7.51cto.com/oss/202203/02/e6bcba676f9e540b50f259f5a6b04e2d3cefb0.png)

  设置 go 代理 并安装

  复制

  ```shell
  go env -w GOPROXY=https://goproxy.cn,direct
  make install 
  ```

  ![img](https://s9.51cto.com/oss/202203/02/6881d7286fc8abb105f212c22f7b7480684bb2.png)

  安装完成

  整体安装目录

  复制

  ```shell
  [root@centos7-01 zabbix]# tree /app/zabbix/
  /app/zabbix/
  ├── bin
  │   └── zabbix_js
  ├── etc
  │   ├── zabbix_agent2.conf
  │   ├── zabbix_agent2.d
  │   │   └── plugins.d
  │   │       ├── ceph.conf
  │   │       ├── docker.conf
  │   │       ├── memcached.conf
  │   │       ├── modbus.conf
  │   │       ├── mongodb.conf
  │   │       ├── mqtt.conf
  │   │       ├── mysql.conf
  │   │       ├── oracle.conf
  │   │       ├── postgres.conf
  │   │       ├── redis.conf
  │   │       └── smart.conf
  │   ├── zabbix_agentd.conf
  │   ├── zabbix_agentd.conf.d
  │   ├── zabbix_server.conf
  │   └── zabbix_server.conf.d
  ├── lib
  │   └── modules
  ├── sbin
  │   ├── zabbix_agent2
  │   ├── zabbix_agentd
  │   └── zabbix_server
  └── share
      ├── man
      │   └── man8
      │       ├── zabbix_agent2.8
      │       └── zabbix_server.8
      └── zabbix
          ├── alertscripts
          └── externalscripts
  ```

  #### php 环境

  Zabbix 6.0 LTS 需要 php 7.2.5 版本以上，需要安装 remi 源支持 php 7.x

  复制

  ```shell
  yum -y install https://rpms.remirepo.net/enterprise/remi-release-7.rpm
  yum install yum-utils
  yum-config-manager --disable 'remi-php*'
  yum-config-manager --enable   remi-php74
  yum -y install php php-fpm
  ```

  ![img](https://s6.51cto.com/oss/202203/02/e47ce5d4188ca2e385a972d8eaa988942cc163.png)

  #### 安装 nginx

  复制

  ```
  yum -y install nginx1.
  ```

  ### Nginx 操作部分

  #### 创建 zabbix 相关配置文件

  首先将 Zabbix 前端文件移动到 /app/zabbix 下

  复制

  ```
  mv /app/ui /app/zabbix
  vim /etc/nginx/conf.d/zabbix.conf1.2.
  ```

  配置文件如下

  复制

  ```
  server {
          listen          80;
  #        server_name     example.com;
          root    /app/zabbix/ui;
          index   index.php;
          location = /favicon.ico {
                  log_not_found   off;
          }
          location / {
                  try_files       $uri $uri/ =404;
          }
          location /assets {
                  access_log      off;
                  expires         10d;
          }
          location ~ /\.ht {
                  deny            all;
          }
          location ~ /(api\/|conf[^\.]|include|locale|vendor) {
                  deny            all;
                  return          404;
          }
          location ~ [^/]\.php(/|$) {
              fastcgi_pass    unix:/run/php-fpm/zabbix.sock;
                  fastcgi_split_path_info ^(.+\.php)(/.+)$;
                  fastcgi_index   index.php;
                  fastcgi_param   DOCUMENT_ROOT   /app/zabbix/ui;
                  fastcgi_param   SCRIPT_FILENAME /app/zabbix/ui$fastcgi_script_name;
                  fastcgi_param   PATH_TRANSLATED /app/zabbix/ui$fastcgi_script_name;
                  include fastcgi_params;
                  fastcgi_param   QUERY_STRING    $query_string;
                  fastcgi_param   REQUEST_METHOD  $request_method;
                  fastcgi_param   CONTENT_TYPE    $content_type;
                  fastcgi_param   CONTENT_LENGTH  $content_length;
                  fastcgi_intercept_errors        on;
                  fastcgi_ignore_client_abort     off;
                  fastcgi_connect_timeout         60;
                  fastcgi_send_timeout            180;
                  fastcgi_read_timeout            180;
                  fastcgi_buffer_size             128k;
                  fastcgi_buffers                 4 256k;
                  fastcgi_busy_buffers_size       256k;
                  fastcgi_temp_file_write_size    256k;
          }
  }1.2.3.4.5.6.7.8.9.10.11.12.13.14.15.16.17.18.19.20.21.22.23.24.25.26.27.28.29.30.31.32.33.34.35.36.37.38.39.40.41.42.43.44.45.
  ```

  #### 修改配置文件

  复制

  ```
  vim /etc/nginx/nginx.conf
  ## listen       80 default_server;
  ## listen       [::]:80 default_server;1.2.3.
  ```

  ![img](https://s3.51cto.com/oss/202203/02/c3fb1ca055141907156240a42b9971b931a1e4.png)

  nginx.conf 加注释

  ![img](https://s2.51cto.com/oss/202203/02/e1b5c7a772d061b802d310aa03825380660546.png)

  zabbix.conf 取消注释

  #### 启动 nginx

  复制

  ```
  systemctl enable nginx 
  systemctl start nginx 1.2.
  ```

  #### 访问前端

  发现前端显示为 502，猜测是由于 php-fpm 没开

  ![img](https://s3.51cto.com/oss/202203/02/f2041d29108e659bdd9835688e46eed8f04f48.png)

  #### 启动 php-fpm

  复制

  ```
  systemctl enable php-fpm
  systemctl start php-fpm1.2.
  ```

  #### 再次访问前端

  发现依然是 502

  ![img](https://s7.51cto.com/oss/202203/02/167e6fa29e10c18d8c1167ce1d486d576a62d9.png)

  #### 查看 nginx 日志

  通过日志可以发现 socket 文件不存在，所以需要创建一个 php-fpm 的配置

  复制

  ```
  tail -f /var/log/nginx/error.log1.
  ```

  ![img](https://s4.51cto.com/oss/202203/02/38fc3f630fb622b9df27748b89d59312b39a84.png)

  #### 创建 zabbix php 相关配置文件

  复制

  ```
  vim /etc/php-fpm.d/zabbix.conf1.
  ```

  复制

  ```
  [zabbix]
  user = apache
  group = apache
  listen = /run/php-fpm/zabbix.sock
  listen.acl_users = apache,nginx
  listen.allowed_clients = 127.0.0.1
  pm = dynamic
  pm.max_children = 50
  pm.start_servers = 5
  pm.min_spare_servers = 5
  pm.max_spare_servers = 35
  pm.max_requests = 200
  php_value[session.save_handler] = files
  php_value[session.save_path]    = /var/lib/php/session
  php_value[max_execution_time] = 300
  php_value[memory_limit] = 128M
  php_value[post_max_size] = 16M
  php_value[upload_max_filesize] = 2M
  php_value[max_input_time] = 300
  php_value[max_input_vars] = 100001.2.3.4.5.6.7.8.9.10.11.12.13.14.15.16.17.18.19.20.
  ```

  ![img](https://s7.51cto.com/oss/202203/02/81674711341a83be1ae548c4bdb946aad30718.png)

  #### 重启 php-fpm 服务

  复制

  ```
  systemctl restart php-fpm1.
  ```

  ![img](https://s4.51cto.com/oss/202203/02/f96d49315efbc9b08fa353f3a0f3d5cdd1ef4d.png)

  访问正常

  ### 前端部分

  #### php 部分安装

  通过前端的诊断，根据报错内容去安装相应的 php 扩展插件

  复制

  ```
  yum -y install php-mbstring php-bcmath php-pgsql php-gd php-xml1.
  ```

  ![img](https://s8.51cto.com/oss/202203/02/49373b8128ce7d46574311c79e4991b36a7f93.png)

  ![img](https://s7.51cto.com/oss/202203/02/21a73c407631ee7218b282577361559a3ccea5.png)

  如不需要LDAP，这个地方可以忽略

  #### 重启相关服务

  复制

  ```
  systemctl restart php-fpm nginx 1.
  ```

  此时可以进入数据库配置部分了

  ![img](https://s6.51cto.com/oss/202203/02/c95534a04e24c28f3ab63900853b5a3da99251.png)

  #### 数据库配置及导入相关文件

  创建用户及数据库

  复制

  ```
  sudo -u postgres createuser --pwprompt zabbix
  sudo -u postgres createdb -O zabbix zabbix1.2.
  ```

  导入数据库相关文件

  复制

  ```
  cat /app/database/postgresql/schema.sql | sudo -u zabbix psql zabbix
  cat /app/database/postgresql/images.sql | sudo -u zabbix psql zabbix
  cat /app/database/postgresql/data.sql | sudo -u zabbix psql zabbix1.2.3.
  ```

  ![img](https://s2.51cto.com/oss/202203/02/386f7f163e4a6d3579083152dc9733fd90db0e.png)

  导入完成

  修改 postgresql 权限文件，将本地权限改为 md5 的验证方式

  ![img](https://s3.51cto.com/oss/202203/02/010709d1390fdf62a46873f573986342cdaf5c.png)

  重启数据库

  复制

  ```
  systemctl restart postgresql-131.
  ```

  #### 前端数据库配置

  这里需要注意的是架构部分填 public 即可，如下图

  ![img](https://s5.51cto.com/oss/202203/02/08d602592bc31a83de873134b3883a927dd5c3.png)

  #### 剩余前端配置

  填写对应时区和实例名称

  ![img](https://s4.51cto.com/oss/202203/02/93aa33497d6d6e5e4dc70972e312b1adbad687.png)

  如果出现下图问题，基本是目录权限问题，碰到此问题修改目录权限或者下载配置文件拷贝至提示目录即可

  ![img](https://s9.51cto.com/oss/202203/02/63ebcde968b0f597401888bdc15ccf095d77c5.png)

  默认用户名密码是Admin/zabbix

  ### ![img](https://s7.51cto.com/oss/202203/02/88f6a855492f134abf1377edc39ef8dc4b09e2.png)

  由于 Zabbix Server 没起来，导致前端显示 Zabbix server 未启动的警告提示

  ![img](https://s2.51cto.com/oss/202203/02/f7f93d316ddfd213ecc576ac4cb6ad5c12d345.png)

  #### Zabbix 配置

  - 程序文件路径为/app/zabbix/sbin/下
  - 配置文件路径为/app/zabbix/etc/下

  #### 制作 Zabbix Server 守护文件

  复制

  ```
  [Unit]
  Description=Zabbix Server
  After=syslog.target
  After=network.target
  After=postgresql.service
  After=pgbouncer.service
  After=postgresql-13.service
  [Service]
  Environment="CONFFILE=/app/zabbix/etc/zabbix_server.conf"
  EnvironmentFile=-/etc/sysconfig/zabbix-server
  Type=forking
  Restart=on-failure
  PIDFile=/tmp/zabbix_server.pid
  KillMode=control-group
  ExecStart=/app/zabbix/sbin/zabbix_server -c $CONFFILE
  ExecStop=/bin/kill -SIGTERM $MAINPID
  RestartSec=10s
  TimeoutSec=0
  [Install]
  WantedBy=multi-user.target1.2.3.4.5.6.7.8.9.10.11.12.13.14.15.16.17.18.19.20.
  ```

  #### 编辑配置文件

  复制

  ```
  vim /app/zabbix/etc/zabbix_server.conf1.
  ```

  ![img](https://s9.51cto.com/oss/202203/02/0178c356645a7ec24f9799ce456c6c9d28f21c.png)

  #### 制作 zabbix agent2 守护文件

  复制

  ```
  [Unit]
  Description=Zabbix Agent 2
  After=syslog.target
  After=network.target
  [Service]
  Environment="CONFFILE=/app/zabbix/etc/zabbix_agent2.conf"
  EnvironmentFile=-/etc/sysconfig/zabbix_agent2
  Type=simple
  Restart=on-failure
  PIDFile=/run/zabbix/zabbix_agent2.pid
  KillMode=control-group
  ExecStart=/app/zabbix/sbin/zabbix_agent2 -c $CONFFILE
  ExecStop=/bin/kill -SIGTERM $MAINPID
  RestartSec=10s
  User=zabbix
  Group=zabbix
  [Install]
  WantedBy=multi-user.target1.2.3.4.5.6.7.8.9.10.11.12.13.14.15.16.17.18.
  ```

  #### 启动组件

  复制

  ```
  systemctl restart zabbix-server zabbix-agent21.
  ```

  ![img](https://s4.51cto.com/oss/202203/02/f314d3b27f7ee55e726363d3ec32c07f634a21.png)

  ![img](https://s7.51cto.com/oss/202203/02/f9fd24187328342ff4251706f7749dfb212362.png)

  #### 最终效果

  ![img](https://s8.51cto.com/oss/202203/02/63112f13957c3a7adee42931908239f48442f0.png)

  首页服务正常

  ![img](https://s9.51cto.com/oss/202203/02/c84484e07966271f37a303aef873d595033652.png)

  图形正常

  ![img](https://s3.51cto.com/oss/202203/02/327f1e981108b12d3eb03411d38db6b0cfd294.png)

  dashboard

  #### 写在最后

  辛苦各位朋友能看到这里了，篇幅是比较长的，如果是完全编译的话更费劲，当然针对一些完全没有外网的朋友相对来说更加的麻烦，有空我会出一个完全离线编译的版本。
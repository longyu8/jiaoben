失败时显示
[root@hadoop22 bin]# redis-cli -h hadoop24 -a pwdtest info replication
Warning: Using a password with '-a' or '-u' option on the command line interface may not be safe.
Could not connect to Redis at hadoop24:6379: Connection refused



userparameter_redis.conf
UserParameter=redis.status[*],/etc/zabbix/alertscripts/check_redis.sh $1


check_redis.sh
#!/bin/bash

# 主机地址/IP
HOST='127.0.0.1'

# 参数是否正确
if [ $# -ne "1" ];then
    echo "arg error!" 
fi

# 获取数据
case $1 in
    role)
        result=`redis-cli -h $HOST -a pwdtest info replication 2>/dev/null |  grep -c role`
        echo $result 
        ;;
	*)
        echo "Usage:$0(role)" 
        ;;
esac

 #redis-cli  -a pwdtest info replication 2>/dev/null 
##### 关于excelize库的一般使用文档已经有很多大佬做了讲解,这里就不画蛇添足,详见文档(https://www.bookstack.cn/read/excelize-2.8-zh/70082f38e765b09d.md),我们就只谈删行时一些遇到的小问题和解决办法:

我们在使用excelize库,删除表中指定的行,一般我们的命令为:

```go
package main

import "github.com/xuri/excelize/v2"

func main() {
	file, err := excelize.OpenFile("testout/西北大区.xlsx")
	if err != nil {
		panic(err)
	}
	defer file.Close()
	err = file.RemoveRow("各门店业绩", 4)
	if err != nil {
		return
	}
}
```

但返回文件后发现并没有生效,是因为没有进行保存操作,需进行一定操作后执行保存才可在文件中生效.

```go
package main

import "github.com/xuri/excelize/v2"

func main() {
	file, err := excelize.OpenFile("testout/西北大区.xlsx")
	if err != nil {
		panic(err)
	}
	defer file.Close()
	err = file.RemoveRow("各门店业绩", 4)
	if err != nil {
		return
	}
    // 要有保存的习惯
	file.Save()
}
```

如果是单独删除一行自然是没有问题的,但如果为循环遍历删除多个行时,便会出现以下情况:

如你想删除第3行,第一次是成功的,但要删除第4行时,却删除了第5行,

```go
for rowIndex, row := range rows {
    if rowIndex > titleRow {
        fmt.Printf("当前行索引: %d\n", rowIndex)
        fmt.Printf("当前行内容: %v\n", row)
        // 遍历每一列
        isOn := false
        for _, cell := range row {
            if cell == s_daq || cell == s_q {
                isOn = true
                break
            }
        }
        if isOn == false {
            file.RemoveRow(sheetName, rowIndex+1)
            fmt.Printf("删除行: %d\n", rowIndex+1)
        }
    }
}
file.Save()
```

是因为我们for中的rowIndex索引为首次捕获的原始数据对应的数列,但经过一次**RemoveRow**()删除操作后,实际文件中对应的行已经被删除,如我们删除了第3行,第4行的数据会自动补齐到第三行,而目标文件的索引也会随之 -1 ,所以导致我们在删除第4行时实际文件却把第5行(与初始文件对比)删除了,所以我们需要做的是添加一个校准值

```go
jiaozhun := 0
for rowIndex, row := range rows {
    if rowIndex > titleRow {
        fmt.Printf("当前行索引: %d\n", rowIndex)
        fmt.Printf("当前行内容: %v\n", row)
        // 遍历每一列
        isOn := false
        for _, cell := range row {
            if cell == s_daq || cell == s_q {
                isOn = true
                break
            }
        }
        if isOn == false {
            file.RemoveRow(sheetName, rowIndex+1-jiaozhun)
            fmt.Printf("删除行: %d\n", rowIndex+1)
            jiaozhun++
        }
    }
}
file.Save()
```

当我们删除原表中的某行,通过校准到现在它所在的行号,即可删除对应的行
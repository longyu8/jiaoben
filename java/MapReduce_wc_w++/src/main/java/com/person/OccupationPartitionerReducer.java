package com.person;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class OccupationPartitionerReducer extends Reducer<Text, Text, Text, Text> {

    @Override
    protected void reduce(Text key, Iterable<Text> values, Context context) throws InterruptedException {
        // 在这个例子中，我们不需要Reducer的逻辑，但为了满足Hadoop API，我们仍然需要定义它
        // 如果真的需要Reducer，可以在这里处理values的迭代
    }
}
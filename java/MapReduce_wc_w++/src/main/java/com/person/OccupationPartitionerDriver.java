package com.person;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class OccupationPartitionerDriver  {

    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf, "Occupation Partitioner");

        job.setJarByClass(OccupationPartitionerDriver.class);

        job.setMapperClass(OccupationPartitionerMapper.class);
        job.setReducerClass(OccupationPartitionerReducer.class); // 如果不需要Reducer，可以设置为null，但这里为了示例保留它
        job.setNumReduceTasks(0); // 设置为0表示不使用Reducer

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

        FileInputFormat.addInputPath(job, new Path(args[0])); // 输入文件路径
        FileOutputFormat.setOutputPath(job, new Path(args[1])); // 输出目录

        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}
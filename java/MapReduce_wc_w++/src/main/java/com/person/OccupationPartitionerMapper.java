package com.person;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import java.io.IOException;

public class OccupationPartitionerMapper extends Mapper<LongWritable, Text, Text, Text> {

    private static final String FIELD_DELIMITER = ",";

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String[] fields = value.toString().split(FIELD_DELIMITER);
        if (fields.length >= 9) { // 确保有足够的字段
            String occupation = fields[8].trim(); // 假设职业字段是第九个字段（索引从0开始）
            Text partitionKey = new Text(occupation.equals("未知") ? "unknown" : "known");
            context.write(partitionKey, value); // 根据职业类型输出到不同分区
        }
    }
}
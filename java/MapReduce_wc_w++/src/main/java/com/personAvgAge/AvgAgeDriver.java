package com.personAvgAge;

import com.OccupationPartitioner.OccupationPartitioner;
import com.personcount.OccupationAgeMapper;
import com.personcount.OccupationCountReducer;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class AvgAgeDriver {

    public static void main(String[] args) throws Exception {
        if (args.length != 2) {
            System.err.println("Usage: AvgAgeDriver <input path> <output path>");
            System.exit(-1);
        }
        Configuration conf = new Configuration();
        Job job  = Job.getInstance(conf, "person Avg");

        job.setJarByClass(AvgAgeDriver.class);
        job.setPartitionerClass(OccupationPartitioner.class);
        job.setNumReduceTasks(1);


        job.setMapperClass(avgAgeMapper.class);
        job.setReducerClass(AvgAgeReducer.class);

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(IntWritable.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        job.setSortComparatorClass(Text.Comparator.class);

        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        System.exit(job.waitForCompletion(true) ? 0 : 1);



    }
}
# 导入OS模块
import os
import time
import threading

from docx.shared import Inches
from win32com import client as wc
import docx

# pip install python-docx

# 待搜索的目录路径
path = f"D:\文档\移动\ceshi\words"
# 待搜索的名称
filename = ".doc"
filenamex = ".docx"


def findfiles(path, filegeshi):
    # 首先遍历当前目录所有文件及文件夹
    file_list = os.listdir(path)
    # 循环判断每个元素是否是文件夹还是文件，是文件夹的话，递归
    # 定义保存结果的数组
    result = []
    for file in file_list:
        # 利用os.path.join()方法取得路径全名，并存入cur_path变量，否则每次只能遍历一层目录
        cur_path = os.path.join(path, file)
        # 判断是否是文件夹
        if os.path.isdir(cur_path):
            # continue
            findfiles(cur_path, filegeshi)
        else:
            # 判断是否是特定文件名称
            if filegeshi in file:
                result.append(file)
    return result


def quitword():
    try:
        word = wc.gencache.EnsureDispatch('word.application')
    except:
        word = wc.gencache.EnsureDispatch('wps.application')
    else:
        word = wc.gencache.EnsureDispatch('kwps.application')
    try:
        word.Documents.Close()
        word.Documents.Close(wc.constants.wdDoNotSaveChanges)
        word.Quit()
    except:
        pass


def doc_to_docx(file):
    print(file)
    file_name = os.path.join(path, file)
    print(file_name)
    # file_new_name = path + '\\' + os.path.splitext(file)[0] + ".docx"
    file_new_name = os.path.join(path, os.path.splitext(file)[0] + '.docx')
    print(file_new_name)
    try:
        word = wc.Dispatch("Word.Application")
    except:
        word = wc.Dispatch("wps.Application")
    else:
        word = wc.Dispatch("kwps.application")

    print("启动word转换")
    #
    doc = word.Documents.Open(file_name)
    print("打开" + file)
    print("before:", file_name)
    doc.SaveAs(file_new_name, 12)  # 12为docx   将file_name文件转换为docx文件存储到file_new_name
    print("after:", file_new_name)
    doc.Close()
    # quitword(wc)
    word.Quit()
    # print(file+"结束!")


# 提取word文档的内容
def fetch_doc(doc_name):
    # print("开始进入图插入")
    doc = docx.Document(doc_name)
    # print("打开docx文件")
    photo_dit_path = f'D:\文档\移动\ceshi\words\电子签名.jpg'
    tables = [table for table in doc.tables]
    # print("拿到tables")
    for table in tables:
        i = len(table.columns)
        if i > 2:
            continue
        else:
            run = table.cell(1, 1).paragraphs[0].add_run()
            run.add_picture(photo_dit_path, width=Inches(0.7))
    doc.save(doc_name)


if __name__ == '__main__':
    # result = findfiles(path, filename)
    # print(result)
    # for file in result:
    #     doc_to_docx(file)
    #     quitword()
    #     time.sleep(1)
    # 调用
    result2 = findfiles(path, filenamex)
    # print(result2)
    # er1 = []
    for file in result2:
        print(file)
        # try:
        fetch_doc(file)
        # except:
        #     er1.append(file)
        #     continue
        time.sleep(0.3)
        print(file + "插入图片完成")
    # print("失败的有:" + str(er1))

import os
from docx import Document

# 定义doc文件夹路径和docx文件夹路径
doc_folder_path = f'D:\文档\移动\ceshi\words\大学物理A-通信2102-学生答卷\\'
docx_folder_path = f'D:\文档\移动\ceshi\words\大学物理A-通信2102-学生答卷\docx\\'

# 获取doc文件夹中所有的doc文件列表
doc_files = [f for f in os.listdir(doc_folder_path) if f.endswith('.doc')]

# 遍历doc文件列表，将每个文件转换为docx文件
for doc_file in doc_files:
    doc_file_path = os.path.join(doc_folder_path, doc_file)
    docx_file_path = os.path.join(docx_folder_path, os.path.splitext(doc_file)[0] + '.docx')
    # 打开doc文件
    doc = Document(doc_file_path)
    # 保存为docx文件
    doc.save(docx_file_path)
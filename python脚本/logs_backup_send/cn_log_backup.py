import os
import shutil
import datetime
from typing import List


def backup_logs():
    # 获取当前日期
    today = datetime.datetime.now().strftime('%Y%m%d')

    # 创建备份目录
    backup_dir = f'/databack/log_backup/{today}/CN_ip_log'
    os.makedirs(backup_dir, exist_ok=True)

    # 找到当前日期的文件并复制到备份目录
    log_files = find_log_files(today)
    for log_file in log_files:
        shutil.copy(log_file, backup_dir)

    # 删除最后修改时间大于180天的目录，以20开头的
    delete_old_directories(today)


def find_log_files(date) -> List[str]:
    log_files = []
    log_dir = '/appdata/goldendb/zxdbproxy1/log'
    for root, dirs, files in os.walk(log_dir):
        for file in files:
            if file.endswith(f'{date}.log'):
                log_files.append(os.path.join(root, file))
    return log_files


# 删除最后修改时间大于180天的目录，以20开头的
def delete_old_directories(date):
    old_dirs = []
    log_dir = '/appdata/goldendb/zxdbproxy1/log'
    for root, dirs, files in os.walk(log_dir):
        for dir in dirs:
            if dir.startswith('20') and (datetime.datetime.now() - datetime.datetime.fromtimestamp(
                    os.path.getmtime(os.path.join(root, dir)))).days > 180:
                old_dirs.append(os.path.join(root, dir))
    for old_dir in old_dirs:
        shutil.rmtree(old_dir)


if __name__ == '__main__':
    backup_logs()

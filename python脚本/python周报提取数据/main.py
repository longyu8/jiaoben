import glob
import os
import json
from datetime import timedelta, datetime
import logging

import numpy as np
import xlwings as xw
import pandas as pd
import yaml
import Read_File
import hebing
import re

import writeBYY
import writeWLY


# 创建Logger

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
# 创建Handler
# 终端Handler
consoleHandler = logging.StreamHandler()
consoleHandler.setLevel(logging.DEBUG)
# 文件Handler
fileHandler = logging.FileHandler('log.log', mode='w', encoding='UTF-8')
fileHandler.setLevel(logging.NOTSET)
# Formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
consoleHandler.setFormatter(formatter)
fileHandler.setFormatter(formatter)
# 添加到Logger中
logger.addHandler(consoleHandler)
logger.addHandler(fileHandler)


def get_diff_time(pdate, days):
    # pdate_obj = datetime.strptime(pdate,"%Y-%m-%d")  #将字符串时间变为日期对象
    time_gap = timedelta(days=days)  # 时间间隔，可以7天也可以其他天数
    pdate_result = pdate - time_gap  # 相减还是一个对象
    return pdate_result.strftime("%Y-%m-%d")  # 调用strftime 变为字符串返回


if __name__ == '__main__':

    # 读取yaml
    f = open('datas.yaml', 'r', encoding='utf-8')
    data = f.read()
    x = yaml.load(data, Loader=yaml.FullLoader)

    #读取csv文件列表
    path = '.'
    extension = 'csv'
    os.chdir(path)
    result = glob.glob('*.{}'.format(extension))
    print(result)
    #判断是否有文件需要合并
    if 'AZ' in str(result):
        logger.debug("包含AZ文件,正在合并ing")
        result = hebing.FileHebing(result)
        logger.debug("合并完成")

    # 写入
    if '边缘云' in str(result):
        writeBYY.daoru(result)
    else:
        print("无边缘云数据,跳过")

    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")

    if '期' in str(result):
        writeWLY.daoru(result)
    else:
        print("无网络云数据,跳过")

    #按任意键退出函数
    import msvcrt
    print()
    print()
    print("===================")
    logger.debug("===================")
    print()
    print()
    print("请按任意键退出~")
    ord(msvcrt.getch())
import logging
from datetime import datetime, timedelta, time
from io import StringIO
import re
import numpy as np
import json

import pandas as pd

# 创建Logger
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
# 创建Handler
# 终端Handler
consoleHandler = logging.StreamHandler()
consoleHandler.setLevel(logging.DEBUG)
# 文件Handler
fileHandler = logging.FileHandler('log.log', mode='w', encoding='UTF-8')
fileHandler.setLevel(logging.NOTSET)
# Formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
consoleHandler.setFormatter(formatter)
fileHandler.setFormatter(formatter)
# 添加到Logger中
logger.addHandler(consoleHandler)
logger.addHandler(fileHandler)

def get_diff_time(pdate, days):
    # pdate_obj = datetime.strptime(pdate,"%Y-%m-%d")  #将字符串时间变为日期对象

    time_gap = timedelta(days=days)  # 时间间隔，可以7天也可以其他天数

    pdate_result = pdate - time_gap  # 相减还是一个对象

    return pdate_result.strftime("%Y-%m-%d")  # 调用strftime 变为字符串返回


# 将list中数值累加后返回
def leijia(num):
    try:
        # num = [pd.to_numeric(re.sub('[,]', '', re.sub('[%]', '', x))) for x in np.nan_to_num(num.values.tolist())]
        num1 = [pd.to_numeric(x) for x in np.nan_to_num(num.values.tolist())]
        max1 = max(num1)
        sum1 = sum(num1)
        avg = sum(num1) / len(num1)
        return [max1, avg, sum1]
        # print(num.dtypes)
    except TypeError as e:
        return 'error'


def read_File(file):
    # 事先定义
    returnJson = {}
    dict01 = {}

    # 获取文件上的日期
    file_date = file.split('-')[4][0:8]
    end_date = datetime.date(pd.to_datetime(file_date, format='%Y-%m-%d', errors='coerce'))
    # print(end_date)
    logger.debug(end_date)

    # print(end_date)
    file_Name = file.split('-')[2]
    # print(file)
    logger.debug(file)
    # print(file_Name)
    if '周' in file:
        try:
            df = pd.read_csv(file, encoding='utf-8', dtype=str).fillna('0')
        except BaseException:
            df = pd.read_csv(file, encoding='ANSI', dtype=str).fillna('0')
        df['开始时间'] = df['开始时间'].str.split(' ',expand=True)[0]
        df = df.replace(['%','\'',','],value='')
        # print(df.values.tolist())
        # print(df)
        df['开始时间'] = pd.to_datetime(df['开始时间'])  # 更改数据类型
        # df = df.set_index('开始时间')

        line = len(df.columns.values)
        # 在这需要改数据格式,否则无法计算
        # df = df.values.tolist()
        biaotou = df.columns.values.tolist()[4:len(df.columns.values)]
        for l2 in biaotou:
            df = df.replace('%', '', regex=True).replace(',', '', regex=True)
            df = df.astype({l2: 'float'})
        # df_all = df.groupby(df['开始时间']).agg(["max", "mean", "sum"]).reset_index()

        df_max = df.groupby(df['开始时间']).max(numeric_only=True)
        df_mean = df.groupby(df['开始时间']).mean(numeric_only=True)
        df_sum = df.groupby(df['开始时间']).sum(numeric_only=True)
        # print(biaotou)
        print(df_max)
        dateA = {}
        dateB = {}
        btA = {}
        # for bt in biaotou:
            # print(bt)
            # btA = {}
        for ri in range(1, 8):
            start_date = get_diff_time(end_date, ri)
            print(start_date)
            for bt in biaotou:
                # list_num = []
                # try:
                # print(df_max.loc[[start_date],bt].values[0])
                amax= df_max.loc[[start_date],bt].values[0]
                amean= df_mean.loc[[start_date],bt].values[0]
                asum= df_sum.loc[[start_date],bt].values[0]
                # except KeyError:
                #     continue
                # print(amax)
                # print("=======================")
                # list_num.append(amax)
                # list_num.append(amean)
                # list_num.append(asum)
                btA[bt] = [amax, amean, asum]
                # btA[bt] = list_num
            # print(btA)
            dateA[start_date] = btA
            print(dateA)
            btA = {}
        # dateA = str(dateA)
        # print(dateA)
        dateB[file_Name] = dateA
        # print(dateB)
        # logger.debug(dateB)
        return dateB

#日数据计算
    else:
        try:
            df = pd.read_csv(file, encoding='utf-8', dtype=str).fillna('0')
        except BaseException:
            df = pd.read_csv(file, encoding='ANSI', dtype=str).fillna('0')
        df['开始时间'] = df['开始时间'].str.split(' ',expand=True)[0]
        # df = df.replace(['%','\'',','],value='')
        # print("222222222222222222")
        # print(df)
        # df = df.replace('%', '', regex=True)
        # df = df.replace(',', '', regex=True)
        df = df.replace('%', '', regex=True).replace(',', '', regex=True)

        # print(df)
        # 取得列数
        # 取得后几列的列名
        lines = df.columns.values.tolist()[4:len(df.columns.values)]
        # print(lines)
        tables = df[lines].fillna('0')
        # print(tables)
        for line in tables.columns.values.tolist():
            # print(tables[line])
            result = leijia(tables[line])
            # print(result)
            logger.debug(result)
            dict01[line] = str(result)
        # print(dict01)

        returnJson[file_Name] = dict01
    # print(returnJson)
    return returnJson
    # print(returnJson)


# read_File('性能管理-历史查询-一期周主机性能-he_cn_weihu-20230106175430.csv')
# read_File2(list)
# print("================================================")
logger.debug("================================================")
# read_File('性能管理-历史查询-一期存储性能-he_cn_weihu-20230111161555.csv')
# read_File('性能管理-历史查询-一期虚机性能AZ1-he_cn_weihu-20230111161132.csv')

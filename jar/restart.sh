#!/bin/bash -l
JARFILE=$(ls -l |grep  .jar | awk '{print$9}')
FILEPATH=$(pwd "$JARFILE")"/"$JARFILE
echo $FILEPATH
PID=$(ps -ef|grep -w "$FILEPATH" | grep -v grep |awk '{printf $2}')

if [ ! -d "./logs" ]; then
  echo $FILEPATH
  mkdir ./logs
fi

if [ ! -n "$PID" ]; then
    echo "pid is null,startup $JARFILE"
    nohup $JAVA_HOME/bin/java -server -Xms256m -Xmx256m -jar -Dserver.port=8655 -Dspring.profiles.active=prod $FILEPATH > log.log 2>&1 &
    echo "startup sucess"
    exit
else
    echo "pid not null, kill ${PID}"
fi

kill -9 ${PID}

if [ $? -eq 0 ];then
    echo "kill $JARFILE success"
    nohup $JAVA_HOME/bin/java -server -Xms256m -Xmx256m -jar -Dserver.port=8655 -Dspring.profiles.active=prod $FILEPATH > log.log 2>&1 &
    echo "startup sucess"
else
    echo "kill $JARFILE fail"
fi

```shell
#!/bin/bash
```

按第六列  重复的删除，并保留一行

```shell
awk '!arr[$6]++' file
```

按第2列和第三  重复的删除，并保留一行

```shell
awk '!arr[$2$3]++'  test.log
awk '!arr[$2_$3]++'  test.log
```

提取两个文件第一列相同的行
```shell
awk -F',' 'NR==FNR{a[$1]=$0;next}NR>FNR{if($1 in a)print $0"\n"a[$1]}' 1.log 2.log

awk 'NR==FNR{a[$1]++}NR>FNR&&a[$1]>1' 111.txt 111.txt

awk 'a[$1]++==1' 

cat 111.txt | awk -F '[:|]' '{print $2}' > 111.txt
```
awk 按某个位置的字符分隔的方法
```shell
awk -F ":" '{ for(i=1;i<=3;i++) printf("%s:",$i)}'
awk -F':' '{print $1 ":" $2 ":" $3; print $4}'
awk -F':' '{print $1 ":" $2 ":" $3; for(i=1;i<=3;i++)$i=""; print}'
```
awk打印用户和密码
```shell
cat test.log  |awk -F '[ ]+' '{print $1 "   " $2}'
```
排序显示重复项目
```shell
cat test.log |awk -F '[ ]+' '{print $1}'| sort | uniq -c | sort -nr

awk -F '\t'来表示分隔符，比如
awk -F '\t' '{print $1}' file1.txt 
```
多个空格分隔的方法
```shell
awk -F '[ ]+' '{print $9}'
ls -lh /etc/sysconfig/network-scripts/ifcfg-* | awk -F '[ ]+' '{print $9}'
```
指定分隔符既可以为空格，又可以为冒号，那么处理将会变得简单。可以使用正则表达式来指定多个分隔符，格式为 -F'[空格:]+' 如下
```shell
awk -F'[ :]+' '{print $NF"\t"$(NF-2)}'  file1.txt 
```
```shell
1、awk '/101/'    file      显示文件file中包含101的匹配行。 
   awk '/101/,/105/'  file 
   awk '$1 == 5'    file 
   awk '$1 == "CT"'    file    注意必须带双引号 
   awk '$1 * $2 >100 '   file  
   awk '$2 >5 && $2<=15'  file
```
```shell
2、awk '{print NR,NF,$1,$NF,}' file     显示文件file的当前记录号、域数和每一行的第一个和最后一个域。 
   awk '/101/ {print $1,$2 + 10}' file       显示文件file的匹配行的第一、二个域加10。 
   awk '/101/ {print $1$2}'  file 
   awk '/101/ {print $1 $2}' file       显示文件file的匹配行的第一、二个域，但显示时域中间没有分隔符。
```

```shell
3、df | awk '$4>1000000 '         通过管道符获得输入，如：显示第4个域满足条件的行。
```
```shell
4、awk -F "|" '{print $1}'   file         按照新的分隔符“|”进行操作。 
   awk  'BEGIN { FS="[: \t|]" } 
   {print $1,$2,$3}'       file         通过设置输入分隔符（FS="[: \t|]"）修改输入分隔符。 

   Sep="|" 
   awk -F $Sep '{print $1}'  file   按照环境变量Sep的值做为分隔符。    
   awk -F '[ :\t|]' '{print $1}' file   按照正则表达式的值做为分隔符，这里代表空格、:、TAB、|同时做为分隔符。 
   awk -F '[][]'    '{print $1}' file   按照正则表达式的值做为分隔符，这里代表[、]
```
```shell
5、awk -f awkfile    file         通过文件awkfile的内容依次进行控制。 
   cat awkfile 
/101/{print "\047 Hello! \047"}    --遇到匹配行以后打印 ' Hello! '.  \047代表单引号。 
{print $1,$2}                      --因为没有模式控制，打印每一行的前两个域。
```
```shell
6、awk '$1 ~ /101/ {print $1}' file         显示文件中第一个域匹配101的行（记录）。
```
```shell
7、awk   'BEGIN { OFS="%"} 
   {print $1,$2}'  file           通过设置输出分隔符（OFS="%"）修改输出格式。
```

```shell
8、awk   'BEGIN { max=100 ;print "max=" max}             BEGIN 表示在处理任意行之前进行的操作。 
   {max=($1 >max ?$1:max); print $1,"Now max is "max}' file           取得文件第一个域的最大值。 
   （表达式1?表达式2:表达式3 相当于： 
   if (表达式1) 
       表达式2 
   else 
       表达式3 
   awk '{print ($1>4 ? "high "$1: "low "$1)}' file 
```
```shell
9、awk '$1 * $2 >100 {print $1}' file         显示文件中第一个域匹配101的行（记录）。
```
```shell
10、awk '{$1 == 'Chi' {$3 = 'China'; print}' file        找到匹配行后先将第3个域替换后再显示该行（记录）。 
    awk '{$7 %= 3; print $7}'  file           将第7域被3除，并将余数赋给第7域再打印。
```
```shell
11、awk '/tom/ {wage=$2+$3; printf wage}' file          找到匹配行后为变量wage赋值并打印该变量。
```
```shell
12、awk '/tom/ {count++;}  
         END {print "tom was found "count" times"}' file            END表示在所有输入行处理完后进行处理。
```
```shell
awk 'gsub(/\$/,"");gsub(/,/,""); cost+=$4;END {print "The total is $" cost>"filename"}' file             gsub函数用空串替换$和,再将结果输出到filename中。 
    1 2 3 $1,200.00 
    1 2 3 $2,300.00 
    1 2 3 $4,000.00 

  awk '{gsub(/\$/,"");gsub(/,/,""); 
  if ($4>1000&&$4<2000) c1+=$4; 
  else if ($4>2000&&$4<3000) c2+=$4; 
  else if ($4>3000&&$4<4000) c3+=$4; 
  else c4+=$4; } 
  END {printf  "c1=[%d];c2=[%d];c3=[%d];c4=[%d]\n",c1,c2,c3,c4}"' file 
  通过if和else if完成条件语句 

  awk '{gsub(/\$/,"");gsub(/,/,""); 
  if ($4>3000&&$4<4000) exit; 
  else c4+=$4; } 
  END {printf  "c1=[%d];c2=[%d];c3=[%d];c4=[%d]\n",c1,c2,c3,c4}"' file 
  通过exit在某条件时退出，但是仍执行END操作。 
  awk '{gsub(/\$/,"");gsub(/,/,""); 
  if ($4>3000) next; 
  else c4+=$4; } 
  END {printf  "c4=[%d]\n",c4}"' file 
  通过next在某条件时跳过该行，对下一行执行操作。 
```
```shell
14、awk '{ print FILENAME,$0 }' file1 file2 file3>fileall              把file1、file2、file3的文件内容全部写到fileall中，格式为 
    打印文件并前置文件名。
```
```shell
15、awk ' $1!=previous { close(previous); previous=$1 }    
    {print substr($0,index($0," ") +1)>$1}' fileall           把合并后的文件重新分拆为3个文件。并与原文件一致。
```
```shell
16、awk 'BEGIN {"date"|getline d; print d}'         通过管道把date的执行结果送给getline，并赋给变量d，然后打印。 
```
```shell
17、awk 'BEGIN {system("echo \"Input your name:\\c\""); getline d;print "\nYour name is",d,"\b!\n"}' 
    通过getline命令交互输入name，并显示出来。 
    awk 'BEGIN {FS=":"; while(getline< "/etc/passwd" >0) { if($1~"050[0-9]_") print $1}}' 
    打印/etc/passwd文件中用户名包含050x_的用户名。 
```
```shell
18、awk '{ i=1;while(i<NF) {print NF,$i;i++}}' file 通过while语句实现循环。 
    awk '{ for(i=1;i<NF;i++) {print NF,$i}}'   file 通过for语句实现循环。     
    type file|awk -F "/" ' 
    { for(i=1;i<NF;i++) 
    { if(i==NF-1) { printf "%s",$i } 
    else { printf "%s/",$i } }}'               显示一个文件的全路径。 
    用for和if显示日期 
    awk  'BEGIN { 
for(j=1;j<=12;j++) 
{ flag=0; 
  printf "\n%d月份\n",j; 
        for(i=1;i<=31;i++) 
        { 
        if (j==2&&i>28) flag=1; 
        if ((j==4||j==6||j==9||j==11)&&i>30) flag=1; 
        if (flag==0) {printf "%02d%02d ",j,i} 
        } 
} 
}'
```

Nginx

统计访问IP次数：
```shell
awk '{a[$1]++}END{for(v in a)print v,a[v]}' access.log
```
统计访问IP次数：
```shell
awk '{a[$1]++}END{for(v in a)print v,a[v]}' access.log
```
统计访问访问大于100次的IP：
```shell
awk '{a[$1]++}END{for(v in a){if(a[v]>100)print v,a[v]}}' access.log
```
统计访问IP次数并排序取前10：
```shell
awk '{a[$1]++}END{for(v in a)print v,a[v]|"sort -k2 -nr |head -10"}' access.log
```
统计时间段访问最多的IP：
```shell
awk '$4>="[02/Jan/2017:00:02:00" && $4<="[02/Jan/2017:00:03:00"{a[$1]++}END{for(v in a)print v,a[v]}' access.log
```
统计上一分钟访问量：
```shell
date=$(date -d '-1 minute' +%d/%d/%Y:%H:%M)
awk -vdate=$date '$4~date{c++}END{print c}' access.log
```
统计访问最多的10个页面：
```shell
awk '{a[$7]++}END{for(v in a)print v,a[v]|"sort -k1 -nr|head -n10"}' access.log
```
统计每个URL数量和返回内容总大小:
```shell
awk '{a[$7]++;size[$7]+=$10}END{for(v in a)print a[v],v,size[v]}' access.log
```
统计每个IP访问状态码数量：
```shell
awk '{a[$1" "$9]++}END{for(v in a)print v,a[v]}' access.log
```
统计访问IP是404状态次数：
```shell
awk '{if($9~/404/)a[$1" "$9]++}END{for(i in a)print v,a[v]}' access.log
```


找出b文件在a文件相同记录：
方法1：
```shell
awk 'FNR==NR{a[$0];next}{if($0 in a)print $0}' a b
3
4
5
awk 'FNR==NR{a[$0];next}{if($0 in a)print FILENAME,$0}' a b
b 3
b 4
b 5
awk 'FNR==NR{a[$0]}NR>FNR{if($0 ina)print $0}' a b
3
4
5
awk 'FNR==NR{a[$0]=1;next}(a[$0]==1)' a b   a[$0]是通过b文件每行获取值，如果是1说明有
awk 'FNR==NR{a[$0]=1;next}{if(a[$0]==1)print}' a b
3
4
5
```
方法2：
```shell
awk 'FILENAME=="a"{a[$0]}FILENAME=="b"{if($0 in a)print $0}' a b
3
4
5
4
5
```
方法3：
```shell
awk 'ARGIND==1{a[$0]=1}ARGIND==2 && a[$0]==1' a b
3
4
5
```

#找出b文件在a文件不同记录：
方法1：
```shell
awk 'FNR==NR{a[$0];next}!($0 in a)' a b
6
7
awk 'FNR==NR{a[$0]=1;next}(a[$0]!=1)' a b
或
awk 'FNR==NR{a[$0]=1;next}{if(a[$0]!=1)print}' a b
6
7
```

方法2：
```shell
awk 'FILENAME=="a"{a[$0]=1}FILENAME=="b" && a[$0]!=1' a b
```
方法3：
```shell
awk 'ARGIND==1{a[$0]=1}ARGIND==2 && a[$0]!=1' a b
```


#将a文件合并到b文件：
方法1：
```shell
awk 'FNR==NR{a[$1]=$0;next}{print a[$1],$2}' a b
```
方法2：
```shell
awk 'FNR==NR{a[$1]=$0}NR>FNR{print a[$1],$2}' a b
```
将a文件相同IP的服务名合并：
```shell
cat a
192.168.1.1: httpd
192.168.1.1: tomcat
192.168.1.2: httpd

awk 'BEGIN{FS=":";OFS=":"}{a[$1]=a[$1] $2}END{for(v in a)print v,a[v]}' a

将第一列合并到一行
awk '{for(i=1;i<=NF;i++)a[i]=a[i]$i" "}END{for(vin a)print a[v]}' file
```
字符串拆分
方法1
```shell
echo "hello" |awk -F '''{for(i=1;i<=NF;i++)print $i}'
h
e
l
l
o
```

方法2：
```shell
echo "hello" |awk '{split($0,a,"''");for(v in a)print a[v]}'
l
o
h
e
l
```

统计字符串中每个字母出现的次数：
```shell
echo "a.b.c,c.d.e" |awk -F'[.,]' '{for(i=1;i<=NF;i++)a[$i]++}END{for(v in a)print v,a[v]}'
a 1
b 1
c 2
d 1
e 1
```
获取某列数字最大数
```shell
awk '{if($1>max)max=$1}END{print max}' file
```
获取第三字段最大值：
```
awk 'BEGIN{max=0}{if($3>max)max=$3}END{print max}' a
```
打印第三字段最大行：
```shell
awk 'BEGIN{max=0}{a[$0]=$3;if($3>max)max=$3}END{for(v in a)if(a[v]==max)print v}'a
```



根据列1,计算同类累加的列3和列4

```shell
cat data.txt
Apple Red 10 2.5  
Banana Yellow 5 1.8  
Apple Green 20 3.2  
Orange Orange 15 2.0  
Banana Green 10 2.2  
Apple Red 10 2.7  
Kiwi Green 8 1.5
```

```shell
awk '{ sum_qty[$1] += $3; sum_weight[$1] += $4 } END { for (fruit in sum_qty) print fruit, sum_qty[fruit], sum_weight[fruit] }' data.txt
```





